const context = AP.context;

function registerWebhooks(webhooks, callback) {
    context.getToken(function (token) {
        fetch(
            '/add-webhooks-to-jira',
            {
                body: JSON.stringify({
                    'url': window.location.origin + '/webhook-received',
                    'webhooks': webhooks
                }),
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "JWT " + token
                }
            }
        ).then(
            function (value) {
                value.json().then(
                    function (responseBody) {
                        if (responseBody['errorCode'] !== undefined) {
                            console.log("Received " + responseBody['errorCode'] + " while trying to register webhooks")
                        } else {
                            const responseArray = responseBody['webhookRegistrationResult'];
                            for (var i = 0; i < responseArray.length; i++) {
                                const webhookRegistrationResult = responseArray[i];
                                if (webhookRegistrationResult.errors != null && webhookRegistrationResult.errors.length > 0) {
                                    const errorMsg = 'Failed to register webhook ' + (i + 1) + ' due to: ' + webhookRegistrationResult['errors'];
                                    toastr.error(errorMsg);
                                    console.log(errorMsg);
                                } else {
                                    callback && callback();
                                    const successMsg = 'Registered webhook ' + (i + 1) + ' with id=' + webhookRegistrationResult['createdWebhookId'];
                                    toastr.success(errorMsg);
                                    console.log(successMsg);
                                }
                            }
                        }
                    }
                )
            },
            function (reason) {
                toastr.error("Failed to register webhooks");
                console.log("Failed to register webhooks", reason);
            }
        );
    });
}


function deleteWebhooks(webhookIds, callback) {
    context.getToken(function (token) {
        fetch(
            '/delete-my-webhooks',
            {
                body: JSON.stringify(webhookIds),
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "JWT " + token
                }
            }
        )
            .then(
                function () {
                    toastr.success('Webhooks deleted successfully!');
                    callback && callback();
                },
                function () {
                    toastr.error('Oops! we could not delete webhooks this time');
                }
            )
            .catch(function () {
                toastr.error('Oops! we could not delete webhooks this time');
            });
    });
}

function getAllWebhooks(callback) {
    context.getToken(function (token) {
        fetch(
            '/list-webhooks',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'JWT ' + token
                }
            }
        ).then(
            function (value) {
                value.json().then(
                    function (responseBody) {
                        callback && callback(responseBody.values);
                    }
                )
            },
            function (reason) {
                console.log('failure', reason);
            }
        );
    });
}

function deleteAllWebhooks(callback) {
    return getAllWebhooks(function (webhooks) {
        const webhookIds = webhooks.map(function (webhook) {
            return webhook.id;
        });
        deleteWebhooks(webhookIds, callback);
    });
}

const webhooksService = {
    registerWebhooks: registerWebhooks,
    getAllWebhooks: getAllWebhooks,
    deleteWebhooks: deleteWebhooks,
    deleteAllWebhooks: deleteAllWebhooks,
};


module.exports = webhooksService;