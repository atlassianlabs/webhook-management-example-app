function createElementFromHTML(htmlString) {
    const div = document.createElement('div');
    div.innerHTML = htmlString.trim();
    return div.firstChild;
}

const utils = {
    createElementFromHTML: createElementFromHTML
};

module.exports = utils;