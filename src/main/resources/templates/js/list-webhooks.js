const webhookService = require('./webhook-service');
const createElementFromHTML = require('./utils').createElementFromHTML;
const tableTemplate = require('./webhook-listing-table.hbs');

const deleteWebhooks = webhookService.deleteWebhooks;
const getAllWebhooks = webhookService.getAllWebhooks;
const deleteAllWebhooks = webhookService.deleteAllWebhooks;

const containerEl = document.getElementById('main-container');
const deleteAllDynamicWebhooksEl = document.getElementById('delete-all-webhooks');

function render(webhooks) {
    containerEl.innerHTML = '';
    containerEl.appendChild(createElementFromHTML(tableTemplate(webhooks)));
}

function attachEventListeners() {
    deleteAllDynamicWebhooksEl.addEventListener('click', function(e) {
        deleteAllWebhooks(render);
    });
    containerEl.addEventListener('click', function (e) {
        if (e.target.id.includes('delete-webhook-')) {
            e.stopPropagation();
            const webhookId = e.target.attributes['webhook-id'].nodeValue;
            deleteWebhooks([webhookId], function () {
                return getAllWebhooks(render)
            });
        }

    }, false);
}

getAllWebhooks(render);
attachEventListeners();



