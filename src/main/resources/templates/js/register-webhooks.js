const webhookService = require('./webhook-service');
const createElementFromHTML = require('./utils').createElementFromHTML;
const myTemplate = require("./register-webhooks-form.hbs");

function registerWebhooks(webhooks, callback) {
    console.log("register");

    AP.context.getToken(function (token) {
        fetch(
            '/add-webhooks-to-jira',
            {
                body: JSON.stringify({
                    'url': window.location.origin + '/webhook-received',
                    'webhooks': webhooks
                }),
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "JWT " + token
                }
            }
        ).then(
            function (value) {
                value.json().then(
                    function (responseBody) {
                        if (responseBody['errorCode'] !== undefined) {
                            console.log("Received " + responseBody['errorCode'] + " while trying to register webhooks")
                        } else {
                            const responseArray = responseBody['webhookRegistrationResult'];
                            for (var i = 0; i < responseArray.length; i++) {
                                const webhookRegistrationResult = responseArray[i];
                                if (webhookRegistrationResult.errors != null && webhookRegistrationResult.errors.length > 0) {
                                    const errorMsg = 'Failed to register webhook ' + (i + 1) + ' due to: ' + webhookRegistrationResult['errors'];
                                    toastr.error(errorMsg);
                                    console.log(errorMsg);
                                } else {
                                    callback && callback();
                                    const successMsg = 'Registered webhook ' + (i + 1) + ' with id=' + webhookRegistrationResult['createdWebhookId'];
                                    toastr.success(successMsg);
                                    console.log(successMsg);
                                }
                            }
                        }
                    }
                )
            },
            function (reason) {
                toastr.error("Failed to register webhooks");
                console.log("Failed to register webhooks", reason);
            }
        );
    });
}

var rows = 0;
const formEL = document.getElementById("form-inputs");
const addButtonEL = document.getElementById("add-new-webhook-button");
const submitButtonEL = document.getElementById("submit");

function addRow(data) {
    console.log(myTemplate(data));
    rows++;
    formEL.appendChild(createElementFromHTML(myTemplate(data)));
}

function attachEventListeners() {
    addButtonEL.addEventListener("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        addRow({rows: rows});
    });
    submitButtonEL.addEventListener("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log("el");
        registerWebhooks(extractRegistrationFormData(), function () {
            // TODO: reroute
            console.log("should re-route to listing page");
        });
    });
}

function init() {
    addRow({rows: rows});
    attachEventListeners();
}

function extractRegistrationFormData() {
    const formData = $("#form").serializeArray();
    const normalisedFormData = formData.reduce(function (result, currentObject) {
        result[currentObject.name] = currentObject['value'];
        return result;
    }, {});
    const webhooks = [];
    for (var i = 0; i < rows; i++) {
        const filterKey = 'jql-filter-' + i;
        const eventAKey = 'event-a-' + i;
        const eventBKey = 'event-b-' + i;
        const eventCKey = 'event-c-' + i;
        const eventDKey = 'event-d-' + i;
        const eventEKey = 'event-e-' + i;
        const eventFKey = 'event-f-' + i;
        webhooks.push({
            jqlFilter: normalisedFormData[filterKey],
            events: [
                normalisedFormData[eventAKey], normalisedFormData[eventBKey], normalisedFormData[eventCKey],
                normalisedFormData[eventDKey], normalisedFormData[eventEKey], normalisedFormData[eventFKey]
            ].filter(function (e) {
                return e !== undefined
            })
        });
    }
    return webhooks;
}

init();






