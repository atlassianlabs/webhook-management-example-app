package com.atlassian.connect.jira;

import com.atlassian.connect.jira.beans.WebhookListResponse;
import com.atlassian.connect.jira.beans.WebhookRegistrationResponseBean;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.IgnoreJwt;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

@Controller
@CrossOrigin(origins = CorsConfiguration.ALL)
public class WebhookController {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @RequestMapping(value = "/manage-webhooks", method = RequestMethod.GET)
    @IgnoreJwt
    public String create() {
        return "manage-webhooks";
    }

    @RequestMapping(value = "/list-webhooks", method = RequestMethod.GET)
    @IgnoreJwt
    public String list() {
        return "list-webhooks";
    }

    @RequestMapping(value = "/webhook-received", method = RequestMethod.POST)
    @ResponseBody
    public String onWebhookReceived(@RequestBody Map<String, Object> request) {
        System.out.println(String.format("Received webhook (%s) from Jira for webhook ids: %s\n\n%s",
                request.get("webhookEvent"),
                request.get("matchedWebhookIds"),
                request.containsKey("issue") ? GSON.toJson(request.get("issue")) : ""));
        return "{\"result\": \"ok\"}";
    }

    @RequestMapping(value = "/add-webhooks-to-jira", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public WebhookRegistrationResponseBean addWebhooksToJira(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> request) {
        return atlassianHostRestClients.authenticatedAsAddon()
                .exchange(
                        hostUser.getHost().getBaseUrl() + "/rest/api/2/webhook",
                        HttpMethod.POST,
                        new HttpEntity<>(request, baseHeaders()),
                        WebhookRegistrationResponseBean.class
                ).getBody();
    }

    @RequestMapping(value = "/list-webhooks", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public WebhookListResponse getMyRegisteredWebhooks(
            @AuthenticationPrincipal AtlassianHostUser hostUser,
            @RequestParam(value = "startAt", defaultValue = "0") Long startAt,
            @RequestParam(value = "startAt", defaultValue = "100") Integer maxResults) {

        final UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(hostUser.getHost().getBaseUrl() + "/rest/api/2/webhook")
                .queryParam("startAt", startAt)
                .queryParam("maxResults", maxResults);

        return atlassianHostRestClients.authenticatedAsAddon()
                .exchange(
                        uri.build().toUri(),
                        HttpMethod.GET,
                        new HttpEntity<>("", baseHeaders()),
                        WebhookListResponse.class
                ).getBody();
    }

    @RequestMapping(value = "/delete-my-webhooks", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String deleteWebhooks(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody List<Long> webhooksToDelete) {
        Map<String, Object> request = ImmutableMap.of("webhookIds", webhooksToDelete);

        try {
            return atlassianHostRestClients.authenticatedAsAddon()
                    .exchange(
                            hostUser.getHost().getBaseUrl() + "/rest/api/2/webhook",
                            HttpMethod.DELETE,
                            new HttpEntity<>(request, baseHeaders()),
                            new ParameterizedTypeReference<String>() {
                            }
                    ).getBody();
        } catch (HttpClientErrorException e) {
            return e.getStatusText();
        }
    }

    private LinkedMultiValueMap<String, String> baseHeaders() {
        LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "application/json");
        return headers;
    }
}
