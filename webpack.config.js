const path = require("path");

var commonConfig = {
    module: {
        rules: [
            {
                test: /\.hbs$/,
                use: [{
                    loader: "handlebars-loader",
                    options: {helperDirs: path.resolve(__dirname, "./js/helpers")}
                }]
            }
        ]
    }
};

var registerWebhooksConfig = Object.assign({}, commonConfig, {
    entry: "./src/main/resources/templates/js/register-webhooks.js",
    output: {
        path: path.resolve(__dirname, "./src/main/resources/static/js"),
        filename: "register-webhooks-bundle.js"
    }
});

var listWebhooksConfig = Object.assign({}, commonConfig, {
    entry: "./src/main/resources/templates/js/list-webhooks.js",
    output: {
        path: path.resolve(__dirname, "./src/main/resources/static/js"),
        filename: "list-webhooks-bundle.js"
    }
});

module.exports = [registerWebhooksConfig, listWebhooksConfig];

