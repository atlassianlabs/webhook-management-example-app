# Webhook app example

## First time setup

    npm install
    npm run build

## Running

Make your app accessible via public internet:

    ngrok http 3000

Make sure you set the base url in the application.yml file.
Then run the AddonApplication class from IntelliJ.

Alternatively, to run it from the terminal:

    mvn spring-boot:run -Drun.arguments="--addon.base-url=https://your-ngrok-here.ngrok.io"

## Front end changes

If you make a change to the front end resources (in src/main/resources/templates), just run:

    npm run build
    
And then recompile the project in IntelliJ (or restart it).

## Contributing

Pull requests are always welcome.


## License

This project is licensed under the [Apache License, Version 2.0](LICENSE.txt).
